#!/bin/sh

SRCDIR="$(dirname "$(readlink -f "$0")")"
LIBDIR="$SRCDIR/lib"

SCM_IS_BUNDLED=false

list_plugins() {
	ls -1 -- "$@" 2> /dev/null |
		sed -e "s|^.*/\([0-9]\+\)-.*|\1 \0|" -e 's/^[^ ]*$/50 \0/' |
		sort -n | cut -d' ' -f2-
}

for x in $(list_plugins "$LIBDIR"/*.sh "$LIBDIR"/*/*.sh); do
	[ -s "$x" ] || continue
	. "$x"
done

unset list_plugins

_scm_main "$@"
