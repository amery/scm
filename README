scm
===

SCM aims to be an agnostic project/subproject manager
somehow mimicing Google's `repo`

Wishlist
--------
* SCM agnostic
** git
** subversion
** mercurial
* optional (magic) git-svn convertion
* nested projects
* track branches, not commits
* command aliases
* optional multiremote
* import tools (env generators)
** repo
** git submodules
** git subtree
** live working tree
** ...
* local (working tree) override of manifests (env)
* global (per user) override of manifests (env)

Commands
--------

* init
* sync
* update (alias up)
* diff
* status (alias st)
* commit (alias ci)
* add (new project to env, add file to parent project)
* rm (delete file, delete project)
* forall
* start (new branch in all projects)
* tag (tag all projects)
* clone
* import
* convert (git-svn)
* info
* dump

Glossary
--------
environment::
	meta project describing how sub projects integrate.

manifest::
	file describing the environment

project::
	software tree with it's own version control

subproject::
	project that lives within another project

working tree::
	place in the filesystem where you have a copy of an environment

Manifests
---------

the manifest is composed by merging the following files in order:

. `$WORKINGTREE/.scm/manifest.ini`
. `$HOME/.scm/config.ini`
. `$WORKINGTREE/.scm/local_manifest.ini`

=== Projects

----
[name]
href = uri
review = uri
integration = uri
other = uri
----

`name` is the path where the project is checked out in the working tree, and therefor unique.

`href` is of the form `[remote/]path[#branch]`. If a `remote` is not provided `"default"` will be used.
This remote will be called `origin`.  If a branch is not provided `"HEAD"` will be used for git,
and `"trunk"` for subversion.

`review` is an _optional_ reference to a patch review site and
`integration` is _optional_ reference to the a continuous integration site.

`other` is an aditional (optional) repository, and the given key will be use as name of the git remote. Please note this is
only compatible with `git` based projects.

=== Review sites

----
[review]
name = uri
----

=== Remote sites

----
[remotes]
name = uri
----

=== Integrations

----
[integrations]
name = uri
----

=== Translations

----
[translate]
pattern = translation
----
