#!/bin/sh

LIBDIR="$(dirname "$0")/../lib"

cat <<EOT
#!/bin/sh
# THIS FILE IS GENERATED - DO NOT EDIT
#

SCM_IS_BUNDLED=true
EOT

list_plugins() {
	ls -1 -- "$@" 2> /dev/null |
		sed -e "s|^.*/\([0-9]\+\)-.*|\1 \0|" -e 's/^[^ ]*$/50 \0/' |
		sort -n | cut -d' ' -f2-
}

for x in $(list_plugins "$LIBDIR"/*.sh "$LIBDIR"/*/*.sh); do
	[ -s "$x" ] || continue
	${SHELL:-/bin/sh} -n "$x" || exit 1

	cat <<-EOT

	# ${x#$LIBDIR/}
	#
	EOT

	cat "$x"
done

cat <<EOT

_scm_main "\$@"
EOT
