.PHONY: all doc clean

PLUGINS=$(wildcard lib/*.sh lib/*/*.sh)
BUNDLE=scm-bundled

all: $(BUNDLE) doc

doc: README.html

clean:
	rm -f $(BUNDLE) README.html *~

$(BUNDLE): scripts/scm-bundle-gen.sh Makefile $(PLUGINS)
	$(SHELL) $< > $@~
	chmod 0755 $@~
	mv $@~ $@

README.html: README
	asciidoc $^
