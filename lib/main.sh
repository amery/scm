_scm_usage() {
	local cmd= aliases= desc=
	if [ $# -gt 0 ]; then
		err "$@"
	fi

	cat <<-EOT >&2
	Usage: $(basename "$0") <command> [args...]

	Commands:
	EOT

	for cmd in $(echo $scm_commands | sort); do
		aliases=$(echo "$scm_command_aliases" | sed -ne "s/\(.*\):$cmd$/\1/p" | tr '\n' ' ')
		eval "desc=\$scm_$(echo "$cmd" | tr '-' '_')_usage_desc"
		if [ -n "$desc" ]; then
			printf " * %-15s: $desc${aliases:+ (alias:${aliases% })}" "$cmd"
		else
			echo " * $cmd${aliases:+ (${aliases% })}"
		fi

	done >&2
	exit 1
}

_scm_main() {
	local cmd=

	[ $# -ne 0 ] || _scm_usage

	for cmd in $scm_commands; do
		if [ "$1" = "$cmd" ]; then
			shift 1

			cmd="scm_$(echo "$cmd" | tr '-' '_')"
			${cmd} "$@"
			exit $?
		fi
	done

	for cmd in $scm_command_aliases; do
		if expr $cmd : "$1:" > /dev/null ; then
			shift 1

			cmd="scm_$(echo "${cmd#*:}" | tr '-' '_')"
			${cmd} "$@"
			exit $?
		fi
	done

	_scm_usage "$1: unknown command"
}
