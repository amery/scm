_scm_git_dirtype_detector() {
	[ ! -e "$1/.git" ] || echo "git"
}

_scm_dirtype_detectors="$_scm_dirtype_detectors _scm_git_dirtype_detector"

_scm_find_git_subprojects() {
	local cb="$1" dir="$2"
	shift 2
	_scm_find_subprojects_scan "$cb" "$dir"/*
}

scm_clone() {
	local shortopts="lsqvno:b:u:c:"
	local longopts="local,no-hardlinks,shared,reference:,quiet,verbose,progress,no-checkout,bare,mirror,origin:,branch:,upload-pack,template:,config:,depth:,no-single-branch,single-branch"
	local options= errno=
	local argn= arn_1=

	options="$(getopt -o "$shortopts" -l "$longopts" -- "$@")"
	errno=$?

	[ $errno -eq 0 ] || exit $errno

	set -x
	eval "set -- $options"
	if [ $# -gt 1 ]; then
		# last two arguments
		eval "argn=\"\$$#\" argn_1=\"\$$(($#-1))\""
	elif [ $# -eq 1 ]; then
		argn="$1"
	fi

	if git clone "$@"; then
		echo "argn_1=$argn_1 argn=$argn"
	fi
}
scm_register_command clone
