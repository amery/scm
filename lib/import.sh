scm_init() {
	local x=
	local LOG_TAG=init

	if [ $# -eq 0 ]; then
		x=$(scm_find_root)
		if [ -n "$x" ]; then
			info "scm root already found at '$x'"
			return 1
		fi
		set -- .
	fi

	[ -d "$1" ] || die "$1: not a directory"
	cd "$1" || die "$1: invalid directory"

	info "$PWD: initializing as scm root"
	mkdir ".scm"

	cd - > /dev/null
}
scm_register_command init

_scm_import_found_subproject() {
	local root="$1" extroot="$2" extrel="$3"

	if [ "$root" != "$SCM_ROOT" ]; then
		root="${root#$SCM_ROOT}"
	else
		root=/
	fi

	if [ -n "$extroot" ]; then
		echo "$root:$extroot:$extrel"
	else
		echo "$root::"
	fi
}

scm_import() {
	local LOG_TAG="import"
	local SCM_ROOT=$(scm_find_root)
	local subprojects="${TMPDIR:-/tmp}/scm-import-subprojects.$$"
	local OLD_IFS="$IFS"
	local d= d2=
	local x= t=

	[ -d "$SCM_ROOT" ] || die "not in an scm environment"

	info "scanning $SCM_ROOT..."
	_scm_find_subprojects "$SCM_ROOT" "_scm_import_found_subproject" > "$subprojects"

	cut -d: -f2 < "$subprojects" | sort -u | grep -v "^$" > "${subprojects}.off-tree"
	while read x; do
		t=$(_scm_detect_dirtype "$x")
		info "off-tree:$t:$x"

		grep ":$x:" "$subprojects" > "${subprojects}.off-tree.current"

		IFS=:
		while read d x d2; do
			info "off-tree: ${d2:+$d2 }-> $d"
		done < "${subprojects}.off-tree.current"
		IFS="$OLD_IFS"

	done < "${subprojects}.off-tree"

	grep "::" "$subprojects" | cut -d: -f1 > "${subprojects}.in-tree"
	while read d; do
		t=$(_scm_detect_dirtype "$SCM_ROOT/$d")
		info "in-tree:$t:$d"
	done < "${subprojects}.in-tree"

	rm -f "$subprojects" "$subprojects".*
}
scm_register_command import
