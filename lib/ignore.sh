_scm_find_ignore_subprojects() {
	false
}

_scm_ignore_dirtype_detector() {
	local scmignore="$SCM_ROOT/.scmignore"
	local x=
	[ -s "$scmignore" ] || return 1

	if ! $_scm_ignore_test_compiled; then
		x="$(sed -e 's/^[ \t]*//' -e '/^#/d;' -e 's/[ \t]*$//' -e '/^$/d;' \
			-e 's/[*]/.*/g' \
			-e 's|^/|^/|' -e 's|^[^^]|^\\\(.*/\\\)\\\?\0|' \
			-e 's|^|-e "|' \
			-e 's|$|\\\(/.*\\\)\\\?$"|' \
			"$scmignore" | tr '\n' ' ')"

		if [ -n "$x" ]; then
			eval "_scm_ignore_test() { grep -q $x; }"
		else
			_scm_ignore_test() {
				cat > /dev/null;
				false;
			}
		fi

		_scm_ignore_test_compiled=true
	fi

	if echo "${1#$SCM_ROOT}" | _scm_ignore_test; then
		echo "ignore"
	fi
}

_scm_ignore_test_compiled=false

_scm_dirtype_detectors="_scm_ignore_dirtype_detector $_scm_dirtype_detectors"
