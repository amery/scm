scm_find_root() {
	local d=

	while true; do
		d="$PWD"
		if [ -d "$d/.scm/" ]; then
			echo "$d"
			return 0
		elif [ "$d" = / ]; then
			break
		elif ! cd ..; then
			break
		fi
	done

	return 1
}
scm_register_command find_root

_scm_detect_dirtype() {
	local d="$1" f= x= t=
	[ -d "$1" ] || return

	for f in $_scm_dirtype_detectors; do
		x=$($f "$d")
		[ -z "$x" ] || t="${t:+$t }$x"
	done

	echo "$t"
}

_scm_find_subprojects_scan() {
	local cb="$1" x= y=
	local z= z0=
	shift

	for x; do
		[ -d "$x" ] || continue
		x="${x%/}"
		if [ -L "$x" ]; then
			# symlink exception
			if expr match "$(readlink -f "$x")" "$(readlink -f "$SCM_ROOT")" > /dev/null; then
				continue
			fi

			# points outside of the environment
			y=$(scm_find_project_root "$x/")
			if [ -n "$y" ]; then
				z=$(cd "$x/" && pwd -P)
				if [ "$y" = "." ]; then
					"$cb" "$x" "$z"
				else
					z0=$(cd "$z/$y/" && pwd -P)
					"$cb" "$x" "$z0" "${z#$z0/}"
				fi
			else
				"$cb" "$x" "!"
			fi

		else
			_scm_find_subprojects "$x" "$cb"
		fi
	done
}

_scm_found_subproject() {
	local root="$1" extroot="$2" extrel="$3"

	if [ "$root" != "$SCM_ROOT" ]; then
		root="${root#$SCM_ROOT/}"
	else
		root=.
	fi

	echo "$root${extroot:+	$extroot${extrel:+	$extrel}}"
}

scm_find_project_root() {
	local d="${1:-.}"
	local LOG_TAG=find_project_root
	local OLD_PWD="$PWD" r=

	cd "$d" && cd "$(pwd -P)" || return 1
	while true; do
		t="$(_scm_detect_dirtype "$PWD")"
		if [ -n "$t" ]; then
			echo "${r:-.}"
			break
		elif [ "$PWD" = "/" ]; then
			break
		else
			cd ..
			r="../$r"
		fi
	done
	cd "$OLD_PWD"
}
scm_register_command find_project_root

_scm_find_subprojects() {
	local d="$1" cb="${2:-_scm_found_subproject}" x=
	local t="$(_scm_detect_dirtype "$d")"

	if [ -n "$t" ]; then
		"$cb" "$d" || return

		for x in $t; do
			_scm_find_${x}_subprojects "$cb" "$d" $(filter_out $x $t)
		done
	else
		_scm_find_subprojects_scan "$cb" "$d"/*/
	fi
}

scm_find_subprojects() {
	local x=$(scm_find_root)
	local SCM_ROOT=$([ -n "$x" ] && cd "$x" && pwd -P)

	if [ -d "$SCM_ROOT" ]; then
		_scm_find_subprojects "$SCM_ROOT"
	fi
}
scm_register_command find_subprojects
