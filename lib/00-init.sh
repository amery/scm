for x in $(set | sed -ne 's|^\(_\?scm_[^ \t=(]\+\)=.*|\1|p'); do
	unset "$x"
done

# commands
#
scm_register_command() {
	local cmd="$1" aliases=
	local x=
	shift

	if [ -n "$cmd" ]; then
		aliases="${cmd#*:}"
		cmd="${cmd%%:*}"

		scm_commands="${scm_commands:+$scm_commands
}$cmd"
		if [ "$cmd" != "$aliases" ]; then
			for x in $(echo "$aliases" | tr ':' ' '); do
				scm_command_aliases="${scm_command_aliases:+$scm_command_aliases
}$x:$cmd"
			done
		fi

		x="scm_$(echo "$cmd" | tr '-' '_')_usage_desc"
		eval "$x=\"$*\""
	fi
}
