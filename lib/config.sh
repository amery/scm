scm_ini() {
	python -c "
import os
import sys

try:
	import ConfigParser as cp
except ImportError:
	import configparser as cp
" "$@"
}
