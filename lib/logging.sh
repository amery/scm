_log() {
	local prefix="$1:${LOG_TAG:+$LOG_TAG:} "
	shift

	if [ $# -eq 0 ]; then
		cat | sed -e "s|^|$prefix|"
	else
		echo "$prefix$*"
	fi >&2
}

info() { _log "I" "$@"; }
warn() { _log "W" "$@"; }
err() { _log "E" "$@"; }
die() { _log "F" "$@"; exit 1; }
