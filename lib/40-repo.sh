_scm_repo_dirtype_detector() {
	[ ! -d "$1/.repo" ] || echo "repo"
}

_scm_dirtype_detectors="$_scm_dirtype_detectors _scm_repo_dirtype_detector"

_scm_find_repo_subprojects() {
	local cb="$1" dir="$2"
	shift 2
	_scm_find_subprojects_scan "$cb" "$dir"/*
}
