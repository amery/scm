filter_out() {
	local x="$1" y=
	shift

	for y; do
		[ "$x" != "$y" ] || echo "$y"
	done
}
